# Gitlab CE / FOSS on Raspberry Pi

## RPi Ubuntu 20.04.2 LTS Install
* Excellent guide for getting RPi installed on a SSD M.2 Drive. I just followed the guide and it was an easy install.
* [RPi Ubuntu 20.04.2 LTS USB Storage guide](https://jamesachambers.com/raspberry-pi-4-ubuntu-20-04-usb-mass-storage-boot-guide/comment-page-3/?unapproved=11143&moderation-hash=16cf99b44d1b08be5fb28fad3ac4ec77#comment-11143)

## GitLab Setup
* Just follow the setup on gitlab's website and everything installs successfully, or it did for me.
* [GitLab Install Ubuntu](https://about.gitlab.com/install/#ubuntu)
